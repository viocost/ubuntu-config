#!/bin/bash

# Function to print usage
print_usage() {
  echo "Usage: source $0 [profile_name]"
  echo "If no profile is specified, uses AWS_PROFILE environment variable or shows fuzzy finder"
  echo "Run with 'eval \$($0)' or source it with '. $0' to export variables"
}

# Get available profiles
AVAILABLE_PROFILES=$(aws configure list-profiles)
if [ $? -ne 0 ]; then
  echo "Error: Failed to list AWS profiles"
  exit 1
fi

# Parse command line arguments
PROFILE=""
if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
  print_usage
  exit 0
fi

# If profile is provided, verify it exists
PROFILE=$1
if [ -n "$PROFILE" ]; then
  if ! echo "$AVAILABLE_PROFILES" | grep -q "^${PROFILE}$"; then
    echo "Profile '$PROFILE' not found in available profiles. Using fuzzy finder..."
    PROFILE=""
  fi
fi

# If no profile specified or invalid profile, check AWS_PROFILE or use fzf
if [ -z "$PROFILE" ]; then
  if [ -n "$AWS_PROFILE" ] && echo "$AVAILABLE_PROFILES" | grep -q "^${AWS_PROFILE}$"; then
    PROFILE=$AWS_PROFILE
  else
    if ! command -v fzf >/dev/null 2>&1; then
      echo "Error: fzf is not installed. Please install it or provide a valid profile name" >&2
      exit 1
    fi
    PROFILE=$(echo "$AVAILABLE_PROFILES" | fzf --height 40% --prompt="Select AWS Profile > ")
    if [ -z "$PROFILE" ]; then
      echo "No profile selected"
      exit 1
    fi
  fi
fi

# Export AWS_PROFILE
export AWS_PROFILE=$PROFILE

# Get the SSO session token
echo "Getting credentials for profile: $PROFILE"
if ! aws sts get-caller-identity --profile "$PROFILE" >/dev/null 2>&1; then
  echo "Current session is invalid or expired. Initiating SSO login..."
  aws sso login --profile "$PROFILE"
  if [ $? -ne 0 ]; then
    echo "Error: SSO login failed"
    #exit 1
  fi
else
  echo "Current session is valid. Proceeding with credential export..."
fi

# Get and parse credentials using jq
CREDS=$(aws configure export-credentials --profile "$PROFILE" 2>/dev/null)
if [ $? -ne 0 ]; then
  echo "Error: Failed to export credentials"
  #exit 1
fi

# Extract and export credentials using jq
export AWS_ACCESS_KEY_ID=$(echo "$CREDS" | jq -r '.AccessKeyId')
export AWS_SECRET_ACCESS_KEY=$(echo "$CREDS" | jq -r '.SecretAccessKey')
export AWS_SESSION_TOKEN=$(echo "$CREDS" | jq -r '.SessionToken')

# Verify credentials were exported
if [ -z "$AWS_ACCESS_KEY_ID" ] || [ -z "$AWS_SECRET_ACCESS_KEY" ] || [ -z "$AWS_SESSION_TOKEN" ]; then
  echo "Error: Failed to parse credentials"
  #exit 1
fi

echo "AWS credentials successfully exported to environment variables:"
echo "AWS_PROFILE=$AWS_PROFILE"
echo "AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID"
echo "AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY"
echo "AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN"
