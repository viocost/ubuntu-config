#!/usr/bin/env bash

# Symbols used in the selection menu
TMUX_SYMBOL=""
DIR_SYMBOL="󰉖"

# Initialize flag variables
dev_spawn=false

# Help message
HELP_MESSAGE="Usage: $(basename "$0") [-d] [session_suffix]
Options:
  -d            Spawn development session
  -h, --help    Display this help message

This script allows you to select a tmux session or directory to open a new tmux session in.
It reads directory patterns from ~/.config/sessionizer/config
You can also specify ignore paths as glob patterns.
"

# Check for --help option
if [[ $1 == "--help" || $1 == "-h" ]]; then
  echo "$HELP_MESSAGE"
  exit 0
fi

# Use getopts to parse the options
while getopts ":dh" opt; do
  case ${opt} in
  d)
    dev_spawn=true
    ;;
  h)
    echo "$HELP_MESSAGE"
    exit 0
    ;;
  \?) ;;
  esac
done

# Read positional arguments after options
shift $((OPTIND - 1))
session_suffix=$1

# Read directory patterns and ignore patterns from config file
config_file="$HOME/.config/sessionizer/config"
dir_patterns=()
ignore_patterns=()

if [[ -f "$config_file" ]]; then
  while IFS= read -r line || [[ -n "$line" ]]; do
    # Remove leading/trailing whitespace
    line="${line#"${line%%[![:space:]]*}"}"
    line="${line%"${line##*[![:space:]]}"}"
    # Skip empty lines and comments
    [[ -z "$line" || "$line" == \#* ]] && continue
    # Check for ignore patterns (lines starting with '!')
    if [[ "$line" == !* ]]; then
      ignore_patterns+=("${line:1}")
    else
      dir_patterns+=("$line")
    fi
  done <"$config_file"
else
  echo "Config file $config_file not found. Continuing without directory search."
fi

# Expand directory patterns to directories
search_dirs=()
for pattern in "${dir_patterns[@]}"; do
  # Expand tilde
  pattern="${pattern/#\~/$HOME}"
  # Expand glob patterns
  expanded_dirs=($(compgen -G "$pattern"))
  search_dirs+=("${expanded_dirs[@]}")
done

# Build the ignore expression for the find command
ignore_expr=()
if [[ ${#ignore_patterns[@]} -gt 0 ]]; then
  for pattern in "${ignore_patterns[@]}"; do
    # Do not expand the pattern; pass it quoted to find
    ignore_expr+=(-path "$pattern" -o)
  done
  # Remove the trailing '-o'
  unset 'ignore_expr[${#ignore_expr[@]}-1]'
fi

# Get the list of tmux sessions and directories
selected=$(
  (
    # List tmux sessions
    tmux ls -F '#{session_id}: #{session_name}' 2>/dev/null | awk -F ": " '{print $2}' | sed "s#^#$TMUX_SYMBOL #"
    # Find directories up to 3 levels deep, applying ignore patterns
    if [[ ${#search_dirs[@]} -gt 0 ]]; then
      if [[ ${#ignore_expr[@]} -gt 0 ]]; then
        find "${search_dirs[@]}" -mindepth 1 -maxdepth 3 -type d \( "${ignore_expr[@]}" \) -prune -o -print 2>/dev/null | sed "s#^#$DIR_SYMBOL #"
      else
        find "${search_dirs[@]}" -mindepth 1 -maxdepth 3 -type d -print 2>/dev/null | sed "s#^#$DIR_SYMBOL #"
      fi
    fi
  ) | fzf
)

if [[ -z $selected ]]; then
  exit 0
fi

selected_item="${selected:2}"
tmux_running=$(pgrep tmux)

if $dev_spawn; then
  dev_session_name="${selected_item##*/}-dev"
  term_session_name="${selected_item##*/}-term"

  if tmux has-session -t="${dev_session_name}" 2>/dev/null || tmux has-session -t="${term_session_name}" 2>/dev/null; then
    echo "One of the sessions already exists"
    exit 1
  fi

  nohup tilix --new-process -e "zsh -c 'tmux new-session -s \"$term_session_name\" -c \"$selected_item\"; exec zsh'" >/dev/null 2>&1 &

  tmux new-session -d -s "$dev_session_name" -c "$selected_item"

  if [[ -z $TMUX ]]; then
    tmux attach-session -t "$dev_session_name"
  else
    tmux switch-client -t "$dev_session_name"
  fi

else

  session_name="${selected_item##*/}"

  if [[ -n $session_suffix ]]; then
    session_name="${session_name}-${session_suffix}"
  fi

  if [[ -z $TMUX ]] && [[ -z $tmux_running ]]; then
    tmux new-session -s "$session_name" -c "$selected_item"
    exit 0
  fi

  if ! tmux has-session -t="${session_name}" 2>/dev/null; then
    if [[ -d "$selected_item" ]]; then
      tmux new-session -ds "${session_name}" -c "$selected_item"
    fi
  fi

  if [[ -z $TMUX ]]; then
    tmux attach -t "$session_name"
  else
    tmux switch-client -t "$session_name"
  fi
fi
