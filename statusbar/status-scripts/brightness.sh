#!/bin/bash

# Set the brightness change step
step=0.1

# Check for flags: -i for increment, -d for decrement
if [ "$1" == "-i" ]; then
	operation="increase"
elif [ "$1" == "-d" ]; then
	operation="decrease"
else
	echo "Usage: $0 [-i | -d]"
	exit 1
fi

# Get the current brightness
current_brightness=$(xrandr --verbose | grep -m 1 -o 'Brightness: [0-9]*\.[0-9]*' | cut -f2 -d ' ')

# Ensure current_brightness is not empty and is a valid number
if ! [[ $current_brightness =~ ^[0-9]+(\.[0-9]+)?$ ]]; then
	echo "Error: Failed to obtain current brightness."
	exit 2
fi

# Calculate new brightness based on the operation
if [ "$operation" == "increase" ]; then
	new_brightness=$(echo "scale=2; $current_brightness + $step" | bc)
elif [ "$operation" == "decrease" ]; then
	new_brightness=$(echo "scale=2; $current_brightness - $step" | bc)
fi

# Ensure new_brightness is within valid range
if [ $(echo "$new_brightness > 1" | bc) -eq 1 ]; then
	new_brightness=1
elif [ $(echo "$new_brightness < 0" | bc) -eq 1 ]; then
	new_brightness=0
fi

echo New brightnewss $new_brightness
# Set the new brightness
xrandr --output eDP-1 --brightness $new_brightness
