#!/bin/bash

# First arg is pacman or yay, second - name of the package
function install() {
  if [[ $1 == apt ]]; then
    COMMAND="sudo $1 install -y $2 "
  elif [[ $1 == snap ]]; then
    COMMAND="sudo $1 install --classic $2 "

  elif [[ $1 == pip ]]; then
    COMMAND="$1 install $2 "
  fi
  $COMMAND

  while [ $? -ne 0 ]; do
    read -p "Error installing $1. Try again? Y/n: " yn

    case $yn in
    [Yy]*) $COMMAND ;;
    [Nn]*) break ;;
    *) echo "Please answer yes or no." ;;
    esac

  done
}

function setup_i3() {
  /usr/lib/apt/apt-helper download-file https://debian.sur5r.net/i3/pool/main/s/sur5r-keyring/sur5r-keyring_2023.02.18_all.deb keyring.deb SHA256:a511ac5f10cd811f8a4ca44d665f2fa1add7a9f09bef238cdfad8461f5239cc4
  sudo apt install ./keyring.deb
  echo "deb http://debian.sur5r.net/i3/ $(grep '^DISTRIB_CODENAME=' /etc/lsb-release | cut -f2 -d=) universe" | sudo tee /etc/apt/sources.list.d/sur5r-i3.list
  sudo apt update
  install apt i3
}

function setup_i3lock_color() {
  git clone https://github.com/Raymo111/i3lock-color.git
  cd i3lock-color
  ./build.sh
  ./install-i3lock-color.sh
  cd ../
  rm -rf i3lock-color
}

function setup_xkb_switch() {
  git clone https://github.com/grwlf/xkb-switch
  cd xkb-switch
  mkdir build
  cd build
  cmake ..
  make
  sudo make install
  make DESTDIR=$HOME/.local install
  sudo ldconfig
  cd ../../
  rm -rf xkb-switch
}

function setup_i3blocks() {
  git clone https://github.com/vivien/i3blocks
  cd i3blocks
  ./autogen.sh
  ./configure
  make
  sudo make install
  cd ../
  rm -rf i3blocks
}

function setup_devour() {
  git clone https://github.com/salman-abedin/devour.git && cd devour && sudo make install
  cd ../
  rm -rf devour
}

# build deps
for i in autoconf libxcb1-dev libxcb-keysyms1-dev libpango1.0-dev libxcb-util0-dev libxcb-icccm4-dev libyajl-dev libstartup-notification0-dev libxcb-randr0-dev libev-dev libxcb-cursor-dev libxcb-xinerama0-dev libxcb-xkb-dev libxkbcommon-dev libxkbcommon-x11-dev autoconf libxcb-xrm0 libxkbfile-dev libxcb-xrm-dev automake libxcb-shape0-dev libxcb-xrm-dev cmake; do
  install apt $i
done

setup_i3
setup_i3blocks
setup_devour
setup_xkb_switch
setup_i3lock_color

# i3 setup
for i in iw steghide xprintidle xdotool rofi tmux tmux-plugin-manager neovim conky picom xfce4-appfinder variety nitrogen arandr ripgrep fd-find zsh zsh-syntax-highlighting neofetch bat lm-sensors xfce4-screenshooter; do
  install apt $i
done

# Install stuff
# yarn ?
for i in chromium-browser vlc tldr docker docker-compose curl remmina gucharmap npm pip tilix kitty sysstat acpi pinta obs-studio sqlite; do
  install apt $i
done

for i in fontawesome i3ipc; do
  install pip $i
done

# pCloud?
for i in drawio skype emacs; do
  install snap $i
done

sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
