#!/bin/env bash
# add fonts
# add russian keyboard
# switch Esc to caps

function makedir() {
	# Add scripts
	if [[ ! -d $1 ]]; then
		mkdir -p $1
	fi
}

function rmquiet() {
	rm $1 -rf 2>/dev/null
}

makedir ~/.local/bin
makedir ~/.config

rmquiet ~/.local/bin/scripts
ln -s $(pwd)/scripts $(readlink -f ~/.local/bin)

rmquiet ~/.zshrc
ln -s $(pwd)/zsh/.zshrc $(readlink -f ~/)

rmquiet ~/.config/aliasrc
ln -s $(pwd)/zsh/aliasrc $(readlink -f ~/.config)

# i3

rmquiet ~/.config/i3
ln -s $(pwd)/config/i3 $(readlink -f ~/.config/i3)

rmquiet ~/.config/i3/i3blocks-top.conf
ln -s $(pwd)/statusbar/i3blocks-top.conf $(readlink -f ~/.config/i3)

rmquiet ~/.config/i3/i3blocks-bottom.conf
ln -s $(pwd)/statusbar/i3blocks-bottom.conf $(readlink -f ~/.config/i3)

rmquiet ~/.config/i3/status-scripts
ln -s $(pwd)/statusbar/status-scripts $(readlink -f ~/.config/i3)

rmquiet ~/.config/assets
ln -s $(pwd)/assets $(readlink -f ~/.config)

rmquiet ~/.config/ranger
ln -s $(pwd)/config/ranger $(readlink -f ~/.config)/ranger

rmquiet ~/.config/nvim
cp -r $(pwd)/config/nvim $(readlink -f ~/.config)/nvim

rmquiet ~/.config/tmux
cp -r $(pwd)/config/tmux $(readlink -f ~/.config)/tmux

rmquiet ~/.path
ln -s $(pwd)/path.txt ~/.path
