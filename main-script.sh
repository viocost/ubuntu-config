#!/bin/bash
#

# Updating arco
./runners/run.sh "sudo apt update -y"

# Installing software
./runners/run.sh ./runners/software.sh

# Configuring git
./runners/run.sh ./runners/git.sh

# Installing fonts
./runners/run.sh ./runners/fonts.sh

# Applying tilix config
./runners/run.sh ./runners/tilix.sh

# Setting up symlinks
./runners/run.sh ./runners/symlinks.sh

# Setting up cron jobs
./runners/run.sh ./runners/cron.sh

# Emacs
./runners/emacs.sh

# Cleaning up
#./runners/run.sh ./runners/cleanup.sh
