#!/usr/bin/env python3

import os
import sys
import subprocess

# Wallpaper directory
wallpaper_dir = "/home/kostia/wallpapers"

# Environment variable file
env_file = "/home/kostia/.config/i3/env.sh"

def set_wallpaper(file):
    subprocess.run(['nitrogen', '--set-zoom-fill', file])
    print(f"Wallpaper set: {file}")

def update_env_file(file):
    with open(env_file, 'r+') as f:
        lines = f.readlines()
        f.seek(0)
        found = False
        for line in lines:
            if line.startswith('export CURRENT_WALLPAPER='):
                found = True
                f.write(f'export CURRENT_WALLPAPER={file}\n')
            else:
                f.write(line)
        if not found:
            f.write(f'export CURRENT_WALLPAPER={file}\n')
        f.truncate()

def find_next_file(file_list, current_file):
    try:
        current_index = file_list.index(current_file)
    except ValueError:
        return None
    next_index = (current_index + 1) % len(file_list)
    return file_list[next_index]

def find_previous_file(file_list, current_file):
    try:
        current_index = file_list.index(current_file)
    except ValueError:
        return None
    previous_index = (current_index - 1) % len(file_list)
    return file_list[previous_index]

def main():
    file_list = sorted([file for file in os.listdir(wallpaper_dir) if file.endswith('.jpg')])
    current_wallpaper = os.getenv('CURRENT_WALLPAPER')

    if not file_list:
        print("No wallpapers found in the specified directory.")
        return
    
    if not current_wallpaper:
        set_wallpaper(os.path.join(wallpaper_dir, file_list[0]))
        update_env_file(os.path.join(wallpaper_dir, file_list[0]))
        return

    if len(sys.argv) > 1:
        if sys.argv[1] == 'next':
            next_wallpaper = find_next_file(file_list, current_wallpaper)
            if next_wallpaper:
                set_wallpaper(os.path.join(wallpaper_dir, next_wallpaper))
                update_env_file(os.path.join(wallpaper_dir, next_wallpaper))
        elif sys.argv[1] == 'previous':
            previous_wallpaper = find_previous_file(file_list, current_wallpaper)
            if previous_wallpaper:
                set_wallpaper(os.path.join(wallpaper_dir, previous_wallpaper))
                update_env_file(os.path.join(wallpaper_dir, previous_wallpaper))
        elif sys.argv[1] == '--file':
            if sys.argv[2] in file_list:
                set_wallpaper(os.path.join(wallpaper_dir, sys.argv[2]))
                update_env_file(os.path.join(wallpaper_dir, sys.argv[2]))

if __name__ == '__main__':
    main()

