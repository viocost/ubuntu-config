case "$1" in
lock)
	#betterlockscreen -l -w blur --off 30
	~/.config/i3/scripts/screensave.sh
	;;
logout)
	i3-msg exit
	;;
suspend)
	lock && systemctl suspend
	;;
hibernate)
	lock && systemctl hibernate
	;;
reboot)
	systemctl reboot
	;;
shutdown)
	systemctl poweroff
	;;
*)
	echo "Usage: $0 {lock|logout|suspend|hibernate|reboot|shutdown}"
	exit 2
	;;
esac

exit 0
