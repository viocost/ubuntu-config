#!/bin/bash

ACTIVE_WIN_NAME=$(i3-msg -t get_tree | jq '.. | select(.focused? == true) | .name')

if [ $(pgrep i3lock | wc -l) -eq 0 ]; then
	if [[ $ACTIVE_WIN_NAME != *\"YouTube\"* ]] && [[ $ACTIVE_WIN_NAME != *\"Zoom\"* ]]; then
		~/.config/i3/scripts/screensave.sh &
	fi
fi
