#!/usr/bin/env bash

# esc to caps
setxkbmap -option caps:escape



# Keyboard layout
setxkbmap -layout us,ru,il
setxkbmap -option 'grp:alt_shift_toggle'

# Duplicate mod key in i3
xmodmap -e 'keycode 135 = Super_R' && xset -r 135

# Path to the directory containing the JPEG wallpaper file
wallpaper_dir="~/wallpapers"

# Path to the Nitrogen executable
nitrogen_executable="nitrogen"

# Iterate over each screen and set wallpaper
for screen in $(xrandr --listmonitors | grep -oP '(?<=\s)[0-9]+(?=:)'); do
    # Get the wallpaper file name for the current screen
    wallpaper_file="${wallpaper_dir}/0305.jpg"

    # Check if the wallpaper file exists
    if [ -f "$wallpaper_file" ]; then
        # Set the wallpaper for the current screen using Nitrogen
        $nitrogen_executable --head=$screen --set-zoom-fill "$wallpaper_file"
        echo "Wallpaper set for screen $screen"
    else
        echo "Wallpaper file not found for screen $screen"
    fi
done
