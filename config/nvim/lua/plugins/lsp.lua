local lspconfig = require("lspconfig")

local function bootstrap_nvm() end

return {
  {
    "williamboman/mason-lspconfig",
    opts = function(_, opts)
      opts.ensure_installed = { "ts_ls" }
      opts.handlers = {
        function(server_name)
          bootstrap_nvm()
          lspconfig[server_name].setup({})
        end,
      }
    end,
  },
  {

    "neovim/nvim-lspconfig",
    opts = {
      -- make sure mason installs the server
      inlay_hints = {
        enabled = true,
        exclude = { "vue" }, -- filetypes for which you don't want to enable inlay hints
      },
      servers = {
        --- @deprecated -- tsserver renamed to ts_ls but not yet released, so keep this for now
        --- the proper approach is to check the nvim-lspconfig release version when it's released to determine the server name dynamically
        tsserver = {
          enabled = false,
        },
        ts_ls = {
          enabled = true,

          filetypes = {
            "javascript",
            "javascriptreact",
            "javascript.jsx",
            "typescript",
            "typescriptreact",
            "typescript.tsx",
          },
        },
        eslint = {
          on_attach = function(client)
            client.server_capabilities.documentFormattingProvider = false
            client.server_capabilities.documentRangeFormattingProvider = false
          end,
        },
        vtsls = {
          enabled = false,
        },
      },
      setup = {
        --- @deprecated -- tsserver renamed to ts_ls but not yet released, so keep this for now
        --- the proper approach is to check the nvim-lspconfig release version when it's released to determine the server name dynamically
        tsserver = function()
          -- disable tsserver
          return true
        end,

        ts_ls = function(_, opts)
          return false
        end,

        vtsls = function()
          return true
        end,
      },
    },
  },
  {
    "m-gail/diagnostic_manipulation.nvim",
    init = function()
      require("diagnostic_manipulation").setup({
        blacklist = {
          require("diagnostic_manipulation.builtin.tsserver").tsserver_codes({ 6133, 6196 }),
        },
        whitelist = {
          -- Your whitelist here
        },
      })
    end,
  },
}
